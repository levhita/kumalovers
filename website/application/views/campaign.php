<?php $this->load->view('campaign/campaign_js')?>

<div class="head-campaign">

	<?php if ($campaign->logo !=''): ?>
    <div id="logo"><a href="<?=$campaign->url?>" title="Home Page"><img class="campaign_logo" src="/images/campaigns/<?=$campaign->logo?>" /></a></div>
	<?php endif;?>
  
	<?php $this->load->view('campaign/add_this')?>
		
  <h1><a href="<?=$campaign->url?>" title="Home Page"><?=$campaign->name?></a></h1>
	
	<div class="description">
		<?=$campaign->description?>
	</div>
	
	  <div class="support_campaign_div">
	  <?php
	    if ( $this->twitter->isAuthed() ) {
	      echo anchor("#", t('Support this Whole Campaign'), array("class" => "support_campaign") );  
	    } else {
	    	echo anchor("#", t('Support this Whole Campaign'), array("class" => "sigin_campaign"));
	    }
	  ?>
	  </div>

    <div class="top_campaign_supporters">
        <?php
          $quantity = $this->campaign->getCampaignSupportersCount($campaign->campaign_id);
          echo anchor("message/$campaign->campaign_id", t('%1% Campaign Supporter(s):', $quantity) );
        ?><br/>
        <div class="avatars">
        <?php
        $top_campaign_supporters = $this->campaign->getTopSupporters($campaign->campaign_id);
        foreach($top_campaign_supporters->result()  AS $row) {
          echo "<a class=\"avatar\" href=\"http://twitter.com/$row->screen_name\" target=\"_blank\" title=\"@$row->screen_name\">
          <img class=\"klout_icon\" src=\"/images/klout.png\"/>
          <span class=\"klout\">".ceil($row->klout)."</span>
          <img class=\"profile_img\" src=\"$row->profile_url\" alt=\"$row->screen_name\"/>
          </a>";
        }?>
        </div>
      </div>
	  <div class="clr"></div>

</div>

<div id="toggle-messages"><a href="#"><?=t('Hide all messages')?></a></div>

<?php $this->load->view('campaign/messages')?>
