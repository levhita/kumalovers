

<div class="main home">
  <h2>Welcome to Crowdter</h2>
  
  <p>This is a beta of the <b>new</b> and <b>revolutionary</b> campaign system. Take a look at
  our current, some for the lulz and others for some legitimate social motivation.</p>
  
  <ul>
  <?php foreach($campaigns->result() AS $campaign):?>
    <li><strong><?= anchor("campaign/$campaign->short_name", $campaign->name)?>:</strong>
  	<p><?= $campaign->short_description ?></p>  
    </li>
  <?php endforeach;?>
  </ul>
</div>

<div class="footer">
<p>If you have an idea for a campaign or just want so say hi, leave us a tweet at
<a href="http://twitter.com/crowdter">@Crowdter</a>.</p>
</div>