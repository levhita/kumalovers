<div class="messages">
	<?php foreach ($messages->result() as $message):?>
		<div class="message clr" id="msg-<?=$message->message_id;?>">
			<div class="tweet">
				<?php
					$string = htmlspecialchars($message->message,0,'UTF-8');
					$string = preg_replace('/((?:f|ht)tp:\/\/[^\s]*)/i', '<a href="$1" target=\"_blank\" rel="nofollow">$1</a>', $string);
					$string = preg_replace('/@(\w+)/', '<a href="http://twitter.com/$1" target=\"_blank\" rel="nofollow">$0</a>', $string);
					echo preg_replace('/#(\w+)/', '<a href="http://search.twitter.com/search?q=%23$1"  target=\"_blank\" rel="nofollow">$0</a>', $string);
				?>
			</div>
			
			<div class="buttons">
			<?php
			if ( $this->twitter->isAuthed() ) {
				/** Shows TweetNow Button for static and messages with TweetNow activated **/
			  if($message->tweet_now=='1' || $message->type == 'static') {
					echo anchor("#", t('Tweet Now'), array("class" => "tweet_button active") );
				}
				
				/** Hides Join Message button for static and already sent buttons **/
				if ($message->type != 'static' && $message->sent == '0' ){
					if(!$this->message->supporterExists($message->message_id, $this->session->userdata('supporter_id')) ) {
						echo anchor("#", t('Support this Message'), array("class" => "join_button add") );
					} else {
	          echo anchor("#", t('Remove me from this Message'), array("class" => "join_button remove") ); 
					}
				}
			} else {
			  /** Hides Sign In to Join Message button for static and already sent buttons **/
			  if ($message->type != 'static' && $message->sent =='0') {
				  echo anchor("#", t('Support this Message'), array("class" => "join_button join") );
			  }

				/** Shows TweetNow Button for static and messages with TweetNow activated **/
			  if($message->tweet_now=='1' || $message->type == 'static') {
			    echo anchor("#", t('Tweet Now'), array("class" => "tweet_button signin") );
				}
			}
			?>
      </div>
      
      <div class="top_supporters">
        <?php
        	$quantity = $this->message->getMessageSupportersCount($message->message_id);
        	echo anchor("message/$message->message_id", t('%1% Supporter(s):', $quantity) );
        ?><br/>
        <?php
        $top_supporters = $this->message->getTopSupporters($message->message_id);
        foreach($top_supporters->result()  AS $row) {
          echo "<a class=\"avatar\" href=\"http://twitter.com/$row->screen_name\" target=\"_blank\" title=\"@$row->screen_name($row->klout Klout)\"><img src=\"$row->profile_url\" alt=\"$row->screen_name\"/></a>";
        }?>
      </div>
      
      <div class="clr"></div>
      
      <div class="help">
      <?php
      if($message->sent=='0') {
	      if ( $message->type=='date' ) {
	      	echo t("To be twitted %1% at %2%", $message->date, $message->time);
	      } else if($message->type=='supporters') {
	      	if ($message->type=='at_time') {
	      		echo t("To be twitted after %1% supporters at %2%", $message->quantity, $message->time);
	      	} else {
	      		echo t("To be twitted at %1% supporters", $message->quantity);
	      	}
	      }
      } else {
      	if( $message->type != 'static') {
      		echo t("Twitted %1% at %2%",  $message->date, $message->time);
      	}
      }
      ?>
      </div>
     
		
    </div>
	<?php endforeach; ?>
</div>