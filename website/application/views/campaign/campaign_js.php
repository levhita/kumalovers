<script type="text/javascript">
function JoinMessage() {
  var current_support = $(this);
  if (confirm('<?=t('A message with this text will be sent from your twitter account.\\r\\nit\\\'s OK for you?')?>') ) {
    $.ajax({
        url: "/process/support/"+$(this).parent().parent().attr("id"),
        context: current_support,
        success: function() {
      	   $(this).html('<?=t('Remove me from this Message')?>');
           current_support.removeClass("add");
           current_support.addClass("remove");
           current_support.unbind('click');
           current_support.bind('click',RemoveFromMessage);
           console.log(current_support);
        }
      });
  }
  return false;
}

function RemoveFromMessage() {
  var current_support = $(this);
  $.ajax({
    url: "/process/unsupport/"+$(this).parent().parent().attr("id"),
    context: current_support,
    success: function() {
       $(this).html('<?=t('Support this Message')?>');
       current_support.removeClass("remove");
       current_support.addClass("add");
       current_support.unbind('click');
       current_support.bind('click', JoinMessage);
       console.log(current_support);
    }
  });
  return false;
}

function TweetNow() {
  var current_support = $(this);
  if( $(this).hasClass('active')) {
	  $.ajax({
      url: "/process/tweet_now/"+$(this).parent().parent().attr("id"),
      context: current_support,
      success: function() {
    	  current_support.removeClass("active");
    	  current_support.addClass("clicked");
        current_support.unbind('click');
        current_support.bind('click', function(){return false;});
      }
    });
  } else if( $(this).hasClass('signin')) {
    if (confirm('<?=t('A message with this text will be sent from your twitter account.\\r\\nit\\\'s OK for you?')?>') ) {
      $(location).attr('href',"/auth/sigin_tweet_now/"+$(this).parent().parent().attr("id"));
    }
	}
  return false;
}

function SupportCampaign() {
	if ( confirm('<?=t('A tweet with EACH MESSAGE of this campaign will be sent from your twitter account.\\r\\nit\\\'s OK for you?')?>') ) {
    $(location).attr('href',"/process/support_all/<?=$campaign->campaign_id?>");
	}
	return false;
}

function SiginCampaign() {
  if ( confirm('<?=t('A tweet with EACH MESSAGE of this campaign will be sent from your twitter account.\\r\\nit\\\'s OK for you?')?>') ) {
    $(location).attr('href',"/auth/sigin_campaign/<?=$campaign->campaign_id?>");
  }
  return false;
}

function SiginMessage() {
  if ( confirm('<?=t('A message with this text will be sent from your twitter account.\\r\\nit\\\'s OK for you?')?>') ) {
    $(location).attr('href',"/auth/sigin_message/"+$(this).parent().parent().attr("id"));
  }
  return false;
}

function ToogleMessages(){
  if ( $('.messages').hasClass('hidden') ) {
	  $('.messages').slideDown('fast');
	  $('.messages').removeClass('hidden');
	  $("#toggle-messages a").html('<?=t('Hide all messages')?>');
  } else {
	  $('.messages').slideUp('fast');
	  $('.messages').addClass('hidden');
	  $("#toggle-messages a").html('<?=t('Show all messages')?>');
	} 
}

$(document).ready(function() {
  $(".support_campaign").bind('click', SupportCampaign);
  $(".sigin_campaign").bind('click', SiginCampaign);
  $(".message .add").bind('click', JoinMessage);
  $(".message .remove").bind('click', RemoveFromMessage);
  $(".message .join").bind('click', SiginMessage);
  $(".message .tweet_button").bind('click', TweetNow);
  $("#toggle-messages a").bind('click', ToogleMessages);
});
</script>