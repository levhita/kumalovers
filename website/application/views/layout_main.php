<html>
<head>
	<?php if($this->config->item('crowdter_custom_name')!=false): ?>
    <title><?= $this->config->item('crowdter_custom_name') ?> :: <?=$title_for_layout?></title>  
	<?php else:?>
    <title>Crowdter :: <?=$title_for_layout?></title>
  <?php endif; ?>
  
  <meta http-equiv="content-type" content="text/html; charset=<?php echo config_item('charset');?>" />
  <link rel="stylesheet/less" type="text/css" href="/css/main.less">
	
  <script src="/js/less.js" type="text/javascript"></script>
	<link href='http://fonts.googleapis.com/css?family=Lato:400,900' rel='stylesheet' type='text/css'/>
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'/>
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans+Mono' rel='stylesheet' type='text/css'/>		
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<!-- <link rel="stylesheet" href="http://www.nikesh.me/demo/loading.css" type="text/css">  -->

	<?php if(isset($custom_css) && $custom_css ):	 ?>
    <style type="text/css">
      <?= $custom_css?>
    </style>
  <?php endif; ?>
  
  <?php if($this->config->item('crowdter_custom_css')!=false): ?>
    <link rel="stylesheet" href="/css/<?= $this->config->item('crowdter_custom_css') ?>" type="text/css">   
  <?php endif; ?>

</head>
<body>
	<div class="page">
		<div class="header">
			
      <h1><?= anchor('/','Crowdter')?></h1>
		  
      <div class="tlogin">      
		    <?php
		    	$this->session->set_userdata('last_page', tmhUtilities::php_self());
			    if($this->twitter->isAuthed()) {
			    	$supporter = $this->supporter->getSupporterById($this->session->userdata("supporter_id"));
			    	echo "@" . $supporter->screen_name . " (" . anchor("auth/logout", t("Logout")).")";	
			    } else {
			    	echo anchor("auth/index/sigin",'<img src="http://a0.twimg.com/images/dev/buttons/sign-in-with-twitter-d.png" alt="" />');
			    }
		      ?><br/>
		    <?= anchor("process/language/spanish", 'Español')?> || <?= anchor("process/language/english", 'English')?>
        <br/>
        <?= anchor('/',t('Home'))?>
		  </div>
		
      <div class="clr"></div>
		
    </div>
		<div class="content">
			<?php echo $content_for_layout?>
		</div>
		
		 <div class="footer outside">
        <div class="side"><p><?= t('Powered by %1%', '<a href="http://crowdter.com/">Crowdter</a>')?>
        <span>ServerTime: <?= date('Y-m-d H:i') ?><br/>
        &copy;2011</span></p></div>
        <div class="wleft">
        <span>This website uses <a href="http://klout.com/">Klout</a> to find out users influence level.</span> 
        </div>
      </div>
      
      <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-27407066-1']);
        _gaq.push(['_trackPageview']);
      
        (function() {
          var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
          ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
      </script>
	</div>
</body>
</html>
