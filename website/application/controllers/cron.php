<?php
	if (! defined('BASEPATH')) exit('No direct script access');
	
	$script_test = substr( $_SERVER['SCRIPT_FILENAME'], -strlen('cli.php'));
	
	if ( $script_test != 'cli.php' ) {
		echo "Forbidden"; 
		exit; 
	}

class Cron extends CI_Controller {
	
	function __construct() {
		parent::__construct();
	    $this->load->library("twitter");
	}
	
	public function tweet($message_id = 1)
	{
		$this->load->model('message');
		
		$message = $this->message->getMessage($message_id);
		$supporters = $this->message->getMessageSupporters($message_id);
		
		$this->message->markAsSent($message_id);
		
		if ( !$supporters->num_rows() ) {
			echo "\nNo Supporters\n";
		} else {
			echo "Message: {$message->message}\n";
			foreach ($supporters->result() as $supporter){
				$this->twitter->tmhOAuth->config['user_token']  = $supporter->token;
				$this->twitter->tmhOAuth->config['user_secret'] = $supporter->token_secret;
				
				if($this->twitter->sendTweet($message->message)) {
					echo $this->twitter->errorMessage();
				} else {
					$this->db->insert('message_log', array('message_id' => $message_id, 'supporter_id' => $supporter->supporter_id)); 
					echo "->{$supporter->screen_name}.\n";
				}
			}
		}
	}
	
  public function sendBurst($quantity=10)
  {
		$this->load->model('message');
		echo date('Y-m-d H:i')."\n";
		if ( !$messages = $this->message->checkSupportersReadyMessages() ) {
			echo "No Supporters Pending Messages\n";
			if ( !$messages = $this->message->checkSupportersAtTimeReadyMessages() ) {
				echo "No Supporters At Time Pending Messages\n";
				if ( !$messages = $this->message->checkTimeReadyMessages() ) {
					echo "No Date Pending Messages\n";
					return;
				}
			}
		}
		
		$message_id = current($messages->result())->message_id;
		$message = $this->message->getMessage($message_id);
		$supporters = $this->message->getNonSentMessageSupporters($message_id);
		
		if ( !$supporters->num_rows() ) {
			echo "No pending pupporters for $message_id, Marking as Sent\n";
			$this->message->markAsSent($message_id);
		} else {
			echo "Message $message_id: {$message->message}\n";
			foreach ($supporters->result() as $supporter){
				$this->twitter->tmhOAuth->config['user_token']  = $supporter->token;
				$this->twitter->tmhOAuth->config['user_secret'] = $supporter->token_secret;
				
				if($this->twitter->sendTweet($message->message)) {
					echo $this->twitter->errorMessage();
				} else {
					 
					/** Mark it as send **/
					$this->db->where('message_supporter_id', $supporter->message_supporter_id);
					$this->db->update('message_supporter', array('sent' => '1')); 
					
					/** Log it **/
					$this->db->insert('message_log', array('message_id' => $message_id, 'supporter_id' => $supporter->supporter_id)); 
					echo "->{$supporter->screen_name}.\n";
				}
			}
		}
	}
	

}