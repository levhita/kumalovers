<?php if (! defined('BASEPATH')) exit('No direct script access');

class Auth extends CI_Controller {
	
	function __construct() {
		parent::__construct();

		// Allow GET parameters, disabled by default
		parse_str($_SERVER['QUERY_STRING'], $_GET);
	}
	
	/**
	 * The entry point to this controller
	 *
	 * @param string $sigin 
	 * @return void
	 * @author demogar
	 */
	public function index($sigin = NULL)
	{
		$here = tmhUtilities::php_self();
		
		if ( $this->twitter->isAuthed() ) { // <- Already logged in
			
			$this->twitter->tmhOAuth->config['user_token']  = $this->session->userdata("a_oauth_token");
			$this->twitter->tmhOAuth->config['user_secret'] = $this->session->userdata("a_oauth_token_secret");
			
			$this->twitter->getUserData();
			$this->supporter->addSupporter($this->twitter->userdata->id, $this->twitter->userdata->screen_name, $this->twitter->userdata->profile_image_url, $this->twitter->userdata->name, $this->session->userdata("a_oauth_token"),$this->session->userdata("a_oauth_token_secret"));
			
			$supporter = $this->supporter->getSupporterByTwitterId($this->twitter->userdata->id);
			$this->session->set_userdata('supporter_id', $supporter->supporter_id);
			
			header("Location: ". $this->session->userdata("last_page") );
		} elseif ( isset($_GET['oauth_verifier']) ) { // <- Coming from Twitter
		  $this->twitter->getAccessToken($_GET['oauth_verifier']);
			header("Location: {$here}" );
		} elseif ( $sigin != NULL ) { // <- Tryinig to log in
			$this->twitter->getOauthToken($here);
		}
	}
	
	/**
	 * Destroys the session
	 *
	 * @author demogar
	 */
	public function logout()
	{
		$this->session->sess_destroy();
		header("Location: ". $this->session->userdata("last_page") );
	}
	
	public function sigin_campaign($campaign_id)
	{
		$this->session->set_userdata('last_page', site_url("/process/support_all/{$campaign_id}"));
		header("Location: ". site_url("/auth/index/sigin"));
	}
	
	public function sigin_message($message_id)
	{
		$this->session->set_userdata('last_page', site_url("/process/support_redirect/{$message_id}"));
		header("Location: ". site_url("/auth/index/sigin"));
	}
	
	public function sigin_tweet_now($message_id)
	{
		$this->session->set_userdata('last_page', site_url("/process/tweet_now_redirect/{$message_id}"));
		header("Location: ". site_url("/auth/index/sigin"));
	}
}