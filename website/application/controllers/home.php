<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		if($this->config->item('crowdter_single_campaign')==false) {
  	  $data = array('title_for_layout'=>'Welcome');
  		$this->load->model('campaign');
  		$data['campaigns'] = $this->campaign->getCampaigns();
  		$this->layout->view('home', $data);
		} else {
		  redirect('/campaign/'.$this->config->item('crowdter_campaign_name').'/', 'refresh');
		}
	}
  
	public function campaign($short_name) {
  	$data = array();
	
    $this->load->model('campaign');
    $this->load->model('message');
    
    $data['campaign'] = $this->campaign->getCampaignFromShortName($short_name);
    if(!count($data['campaign'])) {
    	redirect ('home','location');
    }
    
    $data['messages'] = $this->campaign->getMessages($data['campaign']->campaign_id);
    $data['title_for_layout'] = $data['campaign']->name;
    
    $data['custom_css'] = $this->campaign->getCustomization($data['campaign']->campaign_id);
    
    $this->layout->view('campaign', $data);
	}

}