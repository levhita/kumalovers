<?php if (! defined('BASEPATH')) exit('No direct script access');

class Process extends CI_Controller {
	
	function __construct() {
		parent::__construct();
	}
	
  /**
   * Supports an individual message
   * 
   * @param string $message_id preppended with 'msg-' 
   */
	public function support($message_id)
  {
  	if ( $this->twitter->isAuthed() ) {
	  	$this->load->model('message');
  		$message_id = substr($message_id, strlen('msg-'));
  	  
  		$this->message->addSupporter($message_id, $this->session->userdata('supporter_id'));
	  } else {
  		echo "Not Authed";
  	}
  }
  
  /**
   * Supports an individual and then redirect to selected campaign
   * Used to receive redirect from twitter.
   * @param string $message_id preppended with 'msg-'
   */
  public function support_redirect($message_id)
  {
  	if ( $this->twitter->isAuthed() ) {
	  	$this->load->model('message');
  		$this->load->model('campaign');
	  	
  		$message_id = substr($message_id, strlen('msg-'));
  		  
  		$this->message->addSupporter($message_id, $this->session->userdata('supporter_id'));
  		
  		$campaign =$this->campaign->getMessageCampaign($message_id);	
  		redirect("campaign/$campaign->short_name");
	  } else {
  		echo "Not Authed";
  	}
  }
  
  /**
   * Unsupport message
   * 
   * @param string $message_id preppended with 'msg-'
   */
  public function unsupport($message_id)
  {
  	if ( $this->twitter->isAuthed() ) {
	  	$this->load->model('message');
  		$message_id = substr($message_id, strlen('msg-'));
  	  
	  	$this->message->deleteSupporter($message_id, $this->session->userdata('supporter_id'));
	  } else {
  		echo "Not Authed";
  	}
  }
  
	/**
	 * Support all available campaign messages
	 * 
	 * @param string $message_id preppended with 'msg-'
	 */
  public function support_all($campaign_id)
  {
  	if ( $this->twitter->isAuthed() ) {
	  	$this->load->model('campaign');
  		
	    $this->campaign->addSupporter($campaign_id, $this->session->userdata('supporter_id'));
			
	  	$campaign = $this->campaign->getCampaign($campaign_id);
	  	redirect("campaign/$campaign->short_name");	  
  	} else {
  		echo "Not Authed";
  	}
  }
  
  /**
   * Twitt now button
   * Enter description here ...
   * @param string $message_id preppended with 'msg-
   */
  public function tweet_now($message_id)
  {
  	if ( $this->twitter->isAuthed() ) {
	  	$this->load->model('message');
	  	$message_id = substr($message_id, strlen('msg-'));
  	  
  		$this->db->insert('message_log', array('message_id' => $message_id, 'supporter_id' => $this->session->userdata('supporter_id')));
   		$this->message->tweet($message_id, $this->session->userdata('supporter_id'));
   	} else {
  		echo "Not Authed";
  	}
  }
  
  /**
   * Twitt now button
   * Used to receive redirect from twitter.
   * @param string $message_id preppended with 'msg'
   */
  public function tweet_now_redirect($message_id)
  {
  	if ( $this->twitter->isAuthed() ) {
	  	$this->load->model('message');
	  	$this->load->model('campaign');
	  	
	  	$message_id = substr($message_id, strlen('msg-'));
  	  
  		$this->db->insert('message_log', array('message_id' => $message_id, 'supporter_id' => $this->session->userdata('supporter_id')));
   		$this->message->tweet($message_id, $this->session->userdata('supporter_id'));
   		
   		$campaign =$this->campaign->getMessageCampaign($message_id);	
  		redirect("campaign/$campaign->short_name");
   	} else {
  		echo "Not Authed";
  	}
  }
  
  public function language($language)
  {
		if(file_exists(APPPATH. "language/$language/t_translations.php")) {
    	$this->session->set_userdata('language', $language );
    }
    
    header("Location: ". $this->session->userdata("last_page") );
	}
	
}