<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Translation {

    public function __construct() {
    	global $_translation;
    	$this->CI =& get_instance();
    	
    	$language = $this->CI->session->userdata('language');
    	if( empty( $language ) ) {
    		$language = $this->CI->config->item('language');
    		$this->CI->session->set_userdata('language', $language );
    	}
    	
    	include APPPATH. "language/$language/t_translations.php";
    }
}

function t(){
  global $_translation;
  
  if ( func_num_args()<= 0 ) {
    throw new LengthException('Translation function expects at least one parameter.');
  }
  
  $original = (string)func_get_arg(0);
  $search = array();
  $replace = array();
  
  for ($i = 1;$i < func_num_args();$i++) {
    $search[]   = "%$i%";
    $replace[]  = func_get_arg($i);
  }
  if ( array_key_exists($original, $_translation) ){
    $original = $_translation[$original];
  }
  return str_replace($search, $replace, $original);
}

/* End of file Someclass.php */