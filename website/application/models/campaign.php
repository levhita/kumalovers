<?php if (! defined('BASEPATH')) exit('No direct script access');

class Campaign extends CI_Model {

	public $table = "campaign";	
  public $support_table ="campaign_supporter";
	function __construct() {
		parent::__construct();

	}

	public function getMessages($campaign_id)
	{
		$this->db->order_by("order", "asc"); 
		return $this->db->get_where('message', array('campaign_id'=> $campaign_id, 'active' =>'1'));
	}
	
	public function getUnsentMessages($campaign_id)
	{
		$this->db->order_by("order", "asc"); 
		return $this->db->get_where('message', array('campaign_id'=> $campaign_id, 'active' =>'1', 'sent'=> '0', 'type <>'=> 'static'));
	}
	
	public function getCampaignFromShortName($short_name)
	{
		$query = $this->db->get_where('campaign', array('short_name'=>$short_name),1);
		return $query->row(); 
	}
	
	public function getCampaign($campaign_id)
	{
		$query = $this->db->get_where('campaign', array('campaign_id'=>$campaign_id),1);
		return $query->row();
	}

	public function getMessageCampaign($message_id) {
		$query = $this->db->get_where('message', array('message_id' => $message_id),1);
		$message = $query->row();
		
		$query = $this->db->get_where('campaign', array('campaign_id' => $message->campaign_id),1);
		return $query->row();
	}
	public function getCampaigns($private=false) {
		$conditions= array('active'=>'1', 'private' => '0');
		if ( $private==true ) {
			$conditions['private'] = '1';
		}
		$query = $this->db->get_where('campaign', $conditions);
		return $query;
	}
	
  public function addSupporter($campaign_id, $supporter_id) {
		$this->load->model('message');
  	
		/** Inserts supporter into campaign_supporter **/
		$data = array(
  		"supporter_id"	=> $supporter_id,
  		"campaign_id" 		=> $campaign_id
  	);
  	if ( !$this->supporterExists($campaign_id, $supporter_id) ) {	
			$this->db->insert($this->support_table, $data);
		}
		
		/** Add supporter to each non-sent message **/
		$messages = $this->getUnsentMessages($campaign_id);
  	foreach($messages->result() as $message) {
  		$this->message->addSupporter($message->message_id, $this->session->userdata('supporter_id'));
  	}
  	return true;
  }
  
	public function supporterExists($campaign_id, $supporter_id) {
  	$this->db->select("id")->from($this->support_table)->where(array("campaign_id" => $campaign_id, "supporter_id"=>$supporter_id))->limit(1);
		return ($this->db->count_all_results() > 0) ? TRUE : FALSE;
  }
  
  public function getTopSupporters($campaign_id, $limit=10, $offset=0) {
    $this->db->select('*');
    $this->db->from('supporter');
    $this->db->join('campaign_supporter', 'supporter.supporter_id = campaign_supporter.supporter_id');
    $this->db->where('campaign_supporter.campaign_id', $campaign_id);
    $this->db->order_by('supporter.klout', 'DESC');
    $this->db->limit($limit, $offset);
    return $this->db->get();
  }
  
  public function getCampaignSupportersCount($campaign_id) {
  	$this->db->select('count(*) AS supporters_count')->from('campaign_supporter')->where('campaign_id', $campaign_id);
    $query = $this->db->get();
    $row = $query->row();
    return $row->supporters_count;
  }
  
  public function getCustomization($campaign_id){
    $query = $this->db->get_where('custom', array('campaign_id' => $campaign_id),1);
		if ( !$data = $query->row() ) {
		  return false;
		}
		$custom_css = "
			body {
				background: {$data->body_background};
			}
			.page {
				color: {$data->primary};
				background: {$data->page_background};
			}
			/*
			.header {
				position: fixed;
				top:0px;
				left:0px;
				width:100%;
				height: 30px;
				padding:10px 5px;
  		}
  		.page {
  			margin-top:40px;	
  		}
			.header h1 {
				width: 106px;
				height: 30px;
				margin: -5px 10px;
  		}
			.header h1 a {
				width: 106px;
				height: 30px;
				background: url('/images/sintesix_logo_small.png');
			}
			.header .tlogin {
				font-size:10px;
				margin-right:5px;
				margin-top:3px;
  		}*/
			.head-campaign .description a, .head-campaign h1 a {
				color: {$data->base};
  		}
  		.head-campaign .description a:hover, .head-campaign h1 a, .head-campaign h2 {
				color: {$data->accent};
  		}
  		.footer {
  			
  			
  		}
		";
    return $custom_css; 
  }
}