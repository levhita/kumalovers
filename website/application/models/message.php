<?php if (! defined('BASEPATH')) exit('No direct script access');

class Message extends CI_Model {
	
  public $table = "message";
  public $support_table ="message_supporter";
  function __construct() {
    parent::__construct();
  }
  
  public function getMessage($message_id) {
    $query = $this->db->get_where('message', array('message_id'=> $message_id),1);
    return $query->row();
  }
  
  public function markAsSent($message_id, $timestamp=true) {
  	$data = array('sent' => '1');
  	if ( $timestamp==true ) {
  		$data['date'] = date('Y-m-d');
  		$data['time'] = date('H:i:s');
  	}
  	$this->db->update('message', $data, array('message_id' => $message_id));
	}
  
  public function getMessageCampaign($message_id) {
    $query = $this->db->get_where('message', array('message_id'=> $message_id),1);
    $row = $query->row();
    return $row->campaign_id; 
  }
  
  public function getMessageSupporters($message_id) {
    $this->db->select('*');
    $this->db->from('supporter');
    $this->db->where('message_supporter.message_id', $message_id);
    $this->db->join('message_supporter', 'supporter.supporter_id = message_supporter.supporter_id');
    return $this->db->get();
  }
  
  public function getTopSupporters($message_id) {
    $this->db->select('*');
    $this->db->from('supporter');
    $this->db->where('message_supporter.message_id', $message_id);
    $this->db->join('message_supporter', 'supporter.supporter_id = message_supporter.supporter_id');
    $this->db->order_by('supporter.klout', 'DESC');
    $this->db->limit(14);
    return $this->db->get();
  }
  
  public function getTopTweeters($message_id) {
    $this->db->select('*');
    $this->db->from('supporter');
    $this->db->where('message_log.message_id', $message_id);
    $this->db->join('message_log', 'supporter.supporter_id = message_supporter.supporter_id');
    $this->db->order_by('supporter.klout', 'DESC');
    $this->db->limit(14);
    return $this->db->get();
  }
  
  public function getMessageTweetersCount($message_id) {
  	$this->db->select('count(*) AS supporters_count')->from('message_log')->where('message_id', $message_id);
    $query = $this->db->get();
    $row = $query->row();
    return $row->supporters_count;
  }
  
  public function getMessageSupportersCount($message_id) {
  	$this->db->select('count(*) AS supporters_count')->from('message_supporter')->where('message_id', $message_id);
    $query = $this->db->get();
    $row = $query->row();
    return $row->supporters_count;
  }
  
  public function addSupporter($message_id, $supporter_id) {
		$data = array(
  		"supporter_id"	=> $supporter_id,
  		"message_id" 		=> $message_id
  	);
  	if ( !$this->supporterExists($message_id, $supporter_id) ) {	
			return $this->db->insert($this->support_table, $data);
		}
  	return true;
  }
  
  public function deleteSupporter($message_id, $supporter_id) {
		$data = array(
  		"supporter_id"	=> $supporter_id,
  		"message_id" 		=> $message_id
  	);
  	if ( $this->supporterExists($message_id, $supporter_id) ) {	
			return $this->db->delete($this->support_table, $data);
		}
  	return true;
  }
  
	public function supporterExists($message_id, $supporter_id) {
  	$this->db->select("id")->from($this->support_table)->where(array("message_id" => $message_id, "supporter_id"=>$supporter_id))->limit(1);
		return ($this->db->count_all_results() > 0) ? TRUE : FALSE;
  }
  
  public function tweet($message_id, $supporter_id) {
  	$this->load->model('supporter');
  	$supporter = $this->supporter->getSupporterById($supporter_id);
  	
  	$this->twitter->tmhOAuth->config['user_token']  = $supporter->token;
		$this->twitter->tmhOAuth->config['user_secret'] = $supporter->token_secret;
		
		$message = $this->getMessage($message_id);
  	$this->twitter->sendTweet($message->message);
  }
  
  /**
	 * Checks for date type messages ready to be sent
	 */
	public function checkTimeReadyMessages() {
		$sql = "SELECT message_id
						FROM message
						WHERE sent = '0'
						AND type = 'date'
						AND date <= '" . date("Y-m-d") ."'
						AND time <= '" . date("H:i:s")."'";
		$query = $this->db->query($sql);
		if ( $query->num_rows() > 0 ) {
		  return $query; 
		}
		return false;
	}
	
	/**
	 * Checks for supporters type messages ready to be sent at
	 * an especific time 
	 */
	public function checkSupportersAtTimeReadyMessages() {
										 
		$sql = "SELECT m.message_id
						FROM message AS  m
						JOIN ( 
						        SELECT m.message_id, count(*) AS supporters_count
						        FROM message AS m JOIN message_supporter AS ms
						        ON (m.message_id=ms.message_id)
						        WHERE m.type='supporters'
						        AND m.sent = '0'
						        GROUP BY m.message_id
						    ) AS c
						ON (c.message_id= m.message_id)
						WHERE m.sent = '0'
						AND m.type = 'supporters'
						AND m.at_time = '1'
						AND m.time <= '" . date("H:i:s") . "'
						AND c.supporters_count >= m.quantity";
		$query = $this->db->query($sql);
		if ( $query->num_rows() > 0 ) {
		  return $query; 
		}
		return false;
	}
	
	/**
	 * Checks for supporters type messages ready to be sent
	 */
	public function checkSupportersReadyMessages() {
		$sql = "SELECT m.message_id
						FROM message AS  m
						JOIN ( 
						   SELECT m.message_id, count(*) AS supporters_count
						   FROM message AS m JOIN message_supporter AS ms
						   ON (m.message_id=ms.message_id)
						   WHERE m.type='supporters'
						   AND m.sent = '0'
						   GROUP BY m.message_id
						) AS c
						ON (c.message_id= m.message_id)
						WHERE m.sent = '0'
						AND m.type = 'supporters'
						AND m.at_time = '0'
						AND c.supporters_count >= m.quantity";
		$query = $this->db->query($sql);
		if ( $query->num_rows() > 0 ) {
		  return $query; 
		}
		return false;
	}
	
  public function getNonSentMessageSupporters($message_id, $limit=10, $offset=0) {
    $this->db->select('*');
    $this->db->from('supporter');
    $this->db->join('message_supporter', 'supporter.supporter_id = message_supporter.supporter_id');
    $this->db->where('message_supporter.message_id', $message_id);
    $this->db->where('message_supporter.sent', '0');
    $this->db->limit($limit, $offset);
    return $this->db->get();
  }
		

}
