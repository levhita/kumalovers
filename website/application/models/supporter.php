<?php if (! defined('BASEPATH')) exit('No direct script access');

class Supporter extends CI_Model {
	
	public $table = "supporter";

	function __construct() {
		parent::__construct();
	}
	
	public function addSupporter($twitter_id, $screen_name,$image_url, $name, $token, $token_secret)
	{
		$this->load->library('klout');
		
		$info = $this->klout->get('klout', array('users' => "$screen_name"), 'json');
		
		$klout = 0.0;
		if ( $info->status==200 && isset($info->users[0])) {
        	$klout = $info->users[0]->kscore;
        }
		
       	$data = array(
					"screen_name"	=> $screen_name,
					"name"			=> $name,
					"token"			=> $token,
       		"profile_url"   => $image_url,
					"token_secret"	=> $token_secret,
        	"twitter_id"	=> $twitter_id,
        	"klout" 		=> $klout 
		);

		if ( ! $this->supporterExists($twitter_id) ) {	
			return $this->db->insert($this->table, $data);
		} else {
			$this->db->where(array("screen_name" => $screen_name));
			return $this->db->update($this->table, $data);
		}
	}
	
	public function supporterExists($twitter_id)
	{
		$this->db->select("id")->from($this->table)->where(array("twitter_id" => $twitter_id))->limit(1);
		return ($this->db->count_all_results() > 0) ? TRUE : FALSE;
	}
	
	public function getSupporterByTwitterId($twitter_id) {
		$query = $this->db->get_where($this->table, array("twitter_id" => $twitter_id), 1);
		return $query->row();
	}
	
	public function getSupporterById($supporter_id) {
		$query = $this->db->get_where($this->table, array("supporter_id" => $supporter_id), 1);
		return $query->row();
	}
	
}