<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

$_translation = array(
	'Remove me from this Message' => 'Quitame de este mensaje',
	'Support this Message' => 'Apoya este mensaje',
  
	'A message with this text will be sent from your twitter account.\\r\\nit\\\'s OK for you?'
	=>
  'Un tuit con este texto será enviado desde tu cuenta de twitter\\r\\n¿Estás de acuerdo?',
  
  'A tweet with EACH MESSAGE of this campaign will be sent from your twitter account.\\r\\nit\\\'s OK for you?'
  =>
  'Un tuit con CADA MENSAJE de esta campaña será enviado desde tu cuenta de twitter\\r\\n¿Estás de acuerdo?',
	
  'Sign In to join this message' => 'Inicia sesión para unirte a este mensaje',
  'Sign In to join this Campaign' => 'Inicia sesión para unirte a la campaña completa',
	'Support this Whole Campaign' => 'Apoya la campaña completa',
  'To be twitted %1% at %2%' => 'Será tuiteado el %1% a las %2%',
  'To be twitted after %1% supporters at %2%' => 'Será tuiteando después de alcanzar %1% personas a las %2%',
  '%1% Supporter(s):' => '%1% personas apoyan este mensaje:',
  '%1% Campaign Supporter(s):' => '%1% personas apoyan esta campaña',
  'Powered by %1%' => 'Creado con %1%',
  "Logout" => 'Salir',
  'Tweet Now' => 'Tuitealo Ahora',
  'Twitted %1% at %2%' => 'Este mensaje fue enviado el %1% a las %2%',
  'Show all messages' => 'Mostrar todos los mensajes',
  'Hide all messages' => 'Ocultar todos los mensajes',
  'Home'=>'Inicio',
);