DROP TRIGGER IF EXISTS `{table}_new`;
CREATE TRIGGER `{table}_new` BEFORE INSERT ON `{table}` FOR EACH ROW SET
NEW.created = IFNULL(NEW.created, NOW()),
NEW.updated = IFNULL(NEW.updated, '0000-00-00 00:00:00');

DROP TRIGGER IF EXISTS `{table}_update`;
CREATE TRIGGER `{table}_update` BEFORE UPDATE ON `{table}` FOR EACH ROW SET
NEW.updated = IF(NEW.updated = OLD.updated OR NEW.updated IS NULL, NOW(), NEW.updated),
NEW.created = IFNULL(NEW.created, OLD.created);
