<?php
class AdminModel extends UserModel {
  
  public function __construct($id)
  {
    return parent::__construct('admin', $id);  
  }
}