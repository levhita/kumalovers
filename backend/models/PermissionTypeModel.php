<?php
/**
 * 
 * Enter description here ...
 * @author levhita
 *
 */
class PermissionTypeModel extends RowModel{
  
  protected static $_table_name = 'permission_type';
  protected static $_id_field   = 'permission_type_id';
  
  public function __construct($id)
  {
    return parent::__construct(self::$_table_name, $id);  
  }
}