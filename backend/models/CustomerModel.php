<?php
class CustomerModel extends UserModel {
  
  public function __construct($id)
  {
    return parent::__construct('customer', $id);  
  }
  
  /**
   * If the customer doesnt exist create a new one fi it alreade exists return that
   * @param array $profile_data
   * @return CustomerModel
   */
  public static function CreateCustomer($profile_data, $token, $token_secret) {
    // We take the dato to either create the new user, or update it's info.
    $data = array (
      'name'       => $profile_data->screen_name,
      'fullname'   => $profile_data->name,
    	'twitter_id' => $profile_data->id,
    	'profile_image_url' => $profile_data->profile_image_url,
    	
    	'token' => $token,
    	'token_secret' => $token_secret,
    );
    
    if ( !$Customer = self::getCustomerByTwitterId($profile_data->id) ) {
      $Customer = new CustomerModel(0);
      $Customer->data = $data;
    } else {
      $Customer->data = array_merge($Customer->data, $data);
    }
    $Customer->save();
    return $Customer;
  }
  
  /**
   * @param integer $twitter_id
   * @return CustomerModel
   */
  public static function getCustomerByTwitterId($twitter_id) {
    $DbConnection = DbConnection::getInstance();
    
    $twitter_id = Utils::cleanToDb($twitter_id);
    $sql = "SELECT customer_id FROM customer WHERE twitter_id=$twitter_id";
    
    if( !$customer_id = $DbConnection->getOneValue($sql) ) {
      return FALSE;
    }
    
    $Customer = new CustomerModel((int)$customer_id);
    $Customer->load();
    
    return $Customer;
  }
  
  public function getAllCampaigns() {
    
    $this->assertLoaded();
    $sql = "SELECT campaign_id FROM campaign WHERE customer_id={$this->id}";
    
    if ( !$campaign_ids = $this->DbConnection->getOneColumn($sql) ) {
      return array();
    }
    
    $campaigns = array();
    foreach($campaign_ids AS $campaign_id) {
      $Aux = new CampaignModel((int)$campaign_id);
      $Aux->load();
      $campaigns[] = $Aux;
    }
    
    return $campaigns;
  }
  
  public function getCampaign($campaign_id) {
    $this->assertLoaded();
    $sql = "SELECT customer_id FROM campaign WHERE campaign_id=$campaign_id";
    if ( !$customer_id = $this->DbConnection->getOneValue($sql) ) {
      return false;
    }
    if ( $customer_id != $this->id ) {
      return false;      
    }
    $Campaign = new CampaignModel($campaign_id);
    $Campaign->load();
    return $Campaign;
  }
  
}