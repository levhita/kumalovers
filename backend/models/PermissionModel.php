<?php
/**
 * 
 * Enter description here ...
 * @author levhita
 *
 */
class PermissionModel extends RowModel{
  
  protected static $_table_name = 'permission';
  protected static $_id_field   = 'permission_id';
  
  public function __construct($id)
  {
    return parent::__construct(self::$_table_name, $id);  
  }
  
  public function getPermissionTypeId() {
    return $this->getPermissionTypeIdByPermissionName($this->data['name'], $this->data['actions']);
  }
  
  public function getPermissionTypeIdByPermissionName($name='', $actions="all") {
    $name = Utils::cleanToDb($name);
    $actions = Utils::cleanToDb($actions);
    $sql = "SELECT permission_type_id FROM permission_type WHERE permission='$name' AND actions='$actions'";
    if ( $permission_type_id = $this->DbConnection->getOneValue($sql) ) {
      return $permission_type_id;
    }
    return false;
  }
}