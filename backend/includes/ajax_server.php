<?php
  /** Functions to be added into the xajax server **/  
  $functions = array();
  
  /*
   * Provides a set of function that can be  call trough xajax functions
   * @package ThaFrame
   */
  if ( !defined('TO_ROOT') ) {
    define('TO_ROOT', '..');
    header("Cache-Control: no-cache, must-revalidate");
    ini_set('html_errors', 0);
  }
  require_once TO_ROOT . "/includes/main.inc.php";
  require_once THAFRAME . "/vendors/xajax/xajax.inc.php";
    
  /**
   * Saves a Generic {@link Row} of Data, to be used at catalog
   *
   * @param Array $data
   * @return xajaxResponse
   *
   * @todo Custom error Message
   */
  function saveCatalogRow($data)
  {    
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    
    $DbConnection = DbConnection::getInstance();
    $User = Session::getUser();
    
    $objResponse = new xajaxResponse();
    
    if (!$User->hasPermission('/catalog', all) ) {
      die();
    }
    
    $table_name = $data['table_name'];
    unset($data['table_name']);
   
    if( ! XajaxHelper::saveRow($data, $table_name) ){
      $objResponse->addAlert(   t("No pude guardar los datos: %1% ", $DbConnection->getErrorsString()));
      return $objResponse;
    }
    
    $objResponse->addRedirect("list_table.php?table_name=$table_name");
    return $objResponse;
  }
  $functions[] ='saveCatalogRow';
  
  /**
   * Saves a Generic {@link Row} of Data, to be used at catalog
   *
   * @param Array $data
   * @return xajaxResponse
   *
   * @todo Custom error Message
   */
  function saveClient($data)
  {    
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    
    $DbConnection = DbConnection::getInstance();
    $User = Session::getUser();
    
    $objResponse = new xajaxResponse();
    
    if (!$User->hasPermission('/sales/clients', all) ) {
      die();
    }
    
    $table_name = 'client';
    
    if( ! XajaxHelper::saveRow($data, $table_name) ){
      $objResponse->addAlert(   t("No pude guardar el cliente: %1% ", $DbConnection->getErrorsString()));
      return $objResponse;
    }
    $objResponse->addRedirect("list_clients.php?table_name=$table_name");
    return $objResponse;
  }
  $functions[] ='saveClient';
  
  /**
   * Saves a Generic {@link Row} of Data, to be used at catalog
   *
   * @param Array $data
   * @return xajaxResponse
   *
   * @todo Custom error Message
   */
  function saveCatalogUser($data)
  {    
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    
    $DbConnection = DbConnection::getInstance();
    $User = Session::getUser();
    
    $objResponse = new xajaxResponse();
        
    unset($data['table_name']);
   
    
    if(!empty($data['password']) && !empty($data['password_repeat'])) {
      if($data['password']!=$data['password_repeat']) {
        $objResponse->addAlert(t('Passwords doesn\'t match'));
        return $objResponse;
      } else {
        unset($data['password_repeat']);
        $data['password'] =sha1($data['password']);
      }
    } else if(!empty($data['password']) || !empty($data['password_repeat'])) {
        $objResponse->addAlert(t('You must fill both fields to change the password.'));
        return $objResponse;
    } else {
      unset($data['password_repeat']);
      unset($data['password']);
    }
    
    
    if( ! XajaxHelper::saveRow($data,'user') ){
      $objResponse->addAlert(   t("Couldn't save data: %1% ", $DbConnection->getErrorsString()));
      return $objResponse;
    }
    
    $objResponse->addRedirect("list_table.php?table_name=user");
    return $objResponse;
  }
  $functions[] ='saveCatalogUser';
  
  
    /**
   * Saves a Generic {@link Row} of Data, to be used at catalog
   *
   * @param Array $data
   * @return xajaxResponse
   *
   * @todo Custom error Message
   */
  function saveCatalogPermission($data)
  {    
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    
    $DbConnection = DbConnection::getInstance();
    $User = Session::getUser();
    
    $objResponse = new xajaxResponse();
        
    $PermissionType=new PermissionTypeModel((int)$data['permission_type_id']);
    $PermissionType->load();
    
    $data['name'] = $PermissionType->data['permission'];
    $data['actions'] = $PermissionType->data['actions'];
    
    
    unset($data['permission_type_id']);
    unset($data['table_name']);
    
    if( ! XajaxHelper::saveRow($data,'permission') ){
      $objResponse->addAlert(   t("Couldn't save data: %1% ", $DbConnection->getErrorsString()));
      return $objResponse;
    }
    
    $objResponse->addRedirect( TO_ROOT . "/catalog/list_table.php?table_name=permission");
    return $objResponse;
  }
  $functions[] ='saveCatalogPermission';
  
  /**
   * Deletes a Generic {@link Row} of Data, to be used at catalog
   *
   * @param Array $data
   * @return xajaxResponse
   *
   * @todo Custom error Message
   */
  function deleteCatalogRow($data)
  {    
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    
    $DbConnection = DbConnection::getInstance();
    $User = Session::getUser();
    
    $objResponse = new xajaxResponse();
    
    if (!$User->hasPermission('/catalog', all) ) {
      die();
    }
    
    $table_name = $data['table_name'];
    $id = $data["{$table_name}_id"];
    
    $sql = "UPDATE $table_name SET active='0'  WHERE {$table_name}_id=$id";
    if ( !$DbConnection->executeQuery($sql) ) {
      Logger::log('Couldn\'t delete row', $DbConnection->getErrorsString(), LOGGER_WARNING);
      $objResponse->addAlert( t("Couldn't delete data: %1% ", $DbConnection->getErrorsString()));
      return $objResponse;
    }
    
    $objResponse->addRedirect( TO_ROOT . "/catalog/list_table.php?table_name=$table_name");
    return $objResponse;
  }
  $functions[] ='deleteCatalogRow';
  
    /**
   * Deletes a Generic {@link Row} of Data, to be used at catalog
   *
   * @param Array $data
   * @return xajaxResponse
   *
   * @todo Custom error Message
   */
  function deleteSaleProduct($sale_product_id)
  {    
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    $User = Session::getUser();
    
    $objResponse = new xajaxResponse();
    
    if (!$User->hasPermission('/sales', all) ) {
      die();
    }
        
    $SaleProduct = new RowModel('sale_product',(int)$sale_product_id);
    $SaleProduct->load();
         
    if ( !$SaleProduct->delete() ) {
      $objResponse->addAlert("No se pudo borrar el producto de la venta");
      return $objResponse;
    }
    
    $Sale = new SaleModel((int)$SaleProduct->data['sale_id']);
    $Sale->load();
    $Sale->recalculateTotal();
    $objResponse->assign('total', 'value', $Sale->data['total']);
    
    $objResponse->remove("sale_product_$sale_product_id");
    return $objResponse;
  }
  $functions[] ='deleteSaleProduct';
  
  /**
   * Deletes a Generic {@link Row} of Data, to be used at catalog
   *
   * @param Array $data
   * @return xajaxResponse
   *
   * @todo Custom error Message
   */
  function deleteProductMaterial($product_material_id)
  {    
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    $User = Session::getUser();
    
    $objResponse = new xajaxResponse();
    
    if (!$User->hasPermission('/catalog', all) ) {
      die();
    }
        
    $ProductMaterial = new RowModel('product_material',(int)$product_material_id);
    $ProductMaterial->load();
         
    if ( !$ProductMaterial->delete() ) {
      $objResponse->addAlert("No se pudo borrar el material del producto");
      return $objResponse;
    }
    
    $objResponse->remove("product_material_$product_material_id");
    return $objResponse;
  }
  $functions[] ='deleteProductMaterial';
  
  function openEditProductMaterialDialog($product_material_id, $product_id) {
    $objResponse = new xajaxResponse();
    
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    
    $User = Session::getUser();
    if (!$User->hasPermission('/catalog', all) ) {
      die();
    }
    
    $ProductMaterial = new RowModel('product_material', (int)$product_material_id);
    
    if ( $product_material_id != 0 ) {
      $ProductMaterial->load();
    } else {
      $ProductMaterial->data['product_id'] = $product_id;
    }
    
    $Form = new FormPattern();
    $Form->setRow($ProductMaterial);
    $Form->loadConfig('product_material_catalog', false);
    
    $objResponse->assign('overlay', 'innerHTML', $Form->getAsString() );
    $objResponse->script('showOverlay()');
    
    return $objResponse;
  }
  $functions[] ='openEditProductMaterialDialog';
  
  function openEditSaleProductDialog($sale_product_id, $sale_id) {
    $objResponse = new xajaxResponse();
    
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    
    $User = Session::getUser();
    if (!$User->hasPermission('/sales', all) ) {
      die();
    }
    
    $SaleProduct = new RowModel('sale_product', (int)$sale_product_id);
    
    if ( $sale_product_id != 0 ) {
      $SaleProduct->load();
    } else {
      $SaleProduct->data['sale_id'] = $sale_id;
    }
    
    $Form = new FormPattern();
    $Form->setRow($SaleProduct);
    $Form->loadConfig('sale_product', false);
    $Form->moveBefore('quantity', 'product_id');
    $objResponse->assign('overlay', 'innerHTML', $Form->getAsString() );
    $objResponse->script('showOverlay(); $(\'#quantity\').select()');
    
    return $objResponse;
  }
  $functions[] ='openEditSaleProductDialog';
  
  function openEditPaymentDialog($payment_id, $sale_id) {
    $objResponse = new xajaxResponse();
    
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    
    $User = Session::getUser();
    if (!$User->hasPermission('/payments', all) ) {
      die();
    }
    
    $Payment = new RowModel('payment', (int)$payment_id);
    
    if ( $payment_id != 0 ) {
      $Payment->load();
    } else {
      $Payment->data['sale_id'] = $sale_id;
      $Payment->data['subtotal'] = SaleModel::getPending($sale_id);
    }
    
    $Form = new FormPattern();
    $Form->setRow($Payment);
    $Form->loadConfig('payment', false);
    $objResponse->assign('overlay', 'innerHTML', $Form->getAsString() );
    $objResponse->script('showOverlay();');
    
    return $objResponse;
  }
  $functions[] ='openEditPaymentDialog';
  
  
  $xajax = new xajax(TO_ROOT ."/includes/ajax_server.php");
  
  function saveCatalogProductMaterial($data) {
    $objResponse = new xajaxResponse();
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    $User = Session::getUser();
    if (!$User->hasPermission('/catalog', all) ) {
      die();
    }
    
    if( ! XajaxHelper::saveRow($data,'product_material') ){
      $objResponse->addAlert(   t("No pude guardar el material: %1% ", $DbConnection->getErrorsString()));
      return $objResponse;
    }
    $config = ConfigParser::parsea_mesta(TO_ROOT . '/configs/models/product_catalog_many.yaml');
    $Table= new TablePattern();
    $Table->loadConfig($config['_list_config'], array('product_id'=>$data['product_id']));   
    
    $objResponse->assign('materials', 'innerHTML',$Table->getAsString());
    $objResponse->script('closeOverlay()');
    $objResponse->script('BasicConfig()');
    return $objResponse;
  }
  $functions[] = 'saveCatalogProductMaterial';
  
  function deliverSale($sale_id){
    
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    $User = Session::getUser();
    $objResponse = new xajaxResponse();
    
  	if($sale_id==0) {
      die();
    }
    $Sale = new SaleModel((int)$sale_id);
  	$Sale->load();
  	
  	if ($Sale->data['credit']=='0') {
      $Sale->data['status']='paid';
      $Sale->data['paid']=$Sale->data['total'];
  	} elseif ($Sale->data['credit']=='1') {
      $Sale->data['status']='pending';
  	}
  	
  	if ( !$Sale->save() ){
	  return $objResponse->alert(t("Couldn't Save %1%", $DbConnection->getLastError()));
	}
	
	Session::setMessage('Venta guardada exitosamente', SESSION_MESSAGE_SUCCESS);
  	return $objResponse->redirect("view.php?sale_id=$sale_id"); 
  }
  $functions[]='deliverSale';
  
  function addProductByCode($code, $sale_id){
    $objResponse = new xajaxResponse();
    
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    
    $User = Session::getUser();
    if (!$User->hasPermission('/sales', all) ) {
      die();
    }
    
    if ( !$Product = ProductModel::getByCode($code) ) {
      return $objResponse->alert('Producto Desconocido');
    }
    $SaleProduct = $Product->getSaleProduct($sale_id);
    
    $Sale = new SaleModel((int)$sale_id);
    $Sale->load();
    $price_field = $Sale->getPriceField();
    
    $SaleProduct->data['quantity']++;
    $SaleProduct->data['price'] = $Product->data[$price_field];
    $SaleProduct->data['subtotal'] = $SaleProduct->data['price'] * $SaleProduct->data['quantity'];
    
    if( ! $SaleProduct->save() ){
      $DbConnection = DbConnection::getInstance();
      $objResponse->addAlert("No pude guardar el producto:". $DbConnection->getErrorsString());
      return $objResponse;
    }
    
    $Sale->recalculateTotal();
    
    $config = ConfigParser::parsea_mesta(TO_ROOT . '/configs/models/sale_many.yaml');
    $Table= new TablePattern();
    $Table->loadConfig($config['_list_config'], array('sale_id'=>$sale_id));   
    
    if ( $Sale->data['status'] == 'editing' ) {
      $Table->AddGeneralAction("deliverSale({$sale_id})", 'Entregar', '/images/toolbars/success.png', true);
    }
    
    $objResponse->assign('products', 'innerHTML',$Table->getAsString());
    $objResponse->assign('total', 'value', $Sale->data['total']);
    $objResponse->script('closeOverlay()');
    $objResponse->script('BasicConfig()');
    $objResponse->script("$('#code_search').select();");
    return $objResponse;
  }
  $functions[] = 'addProductByCode';
  
  
  function saveSaleProduct($data) {
    $objResponse = new xajaxResponse();
    
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    
    $User = Session::getUser();
    if (!$User->hasPermission('/sales', all) ) {
      die();
    }
    
    if ( empty($data['product_id']) ) {
      return $objResponse->alert('Primero debe seleccionar un producto');
    }
    $config = ConfigParser::parsea_mesta(TO_ROOT . '/configs/models/sale_many.yaml');
    
    $Product = new RowModel('product',(int)$data['product_id']);
    $Product->load();
    
    $Sale = new SaleModel((int)$data['sale_id']);
    $Sale->load();
    $price_field = $Sale->getPriceField();
    
    $data['price'] = $Product->data[$price_field];
    $data['subtotal'] = $data['price'] * $data['quantity'];
    
    if( ! XajaxHelper::saveRow($data,'sale_product') ){
      $objResponse->addAlert("No pude guardar el producto:". $DbConnection->getErrorsString());
      return $objResponse;
    }
    
    $Sale->recalculateTotal();
    
    $Table= new TablePattern();
    $Table->loadConfig($config['_list_config'], array('sale_id'=>$data['sale_id']));   
    if ( $Sale->data['status'] == 'editing' ) {
      $Table->AddGeneralAction("deliverSale({$Sale->data['sale_id']})", 'Entregar', '/images/toolbars/success.png', true);
    }
    
    $objResponse->assign('products', 'innerHTML',$Table->getAsString());
    $objResponse->assign('total', 'value', $Sale->data['total']);
    $objResponse->script('closeOverlay()');
    $objResponse->script('BasicConfig()');
    return $objResponse;
  }
  $functions[] = 'saveSaleProduct';
  
  
  function saveSection($data) {
    $objResponse = new xajaxResponse();
    
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    
    $User = Session::getUser();
    if (!$User->hasPermission('/', all) ) {
      die();
    }
    
    if( ! XajaxHelper::saveRow($data,'section') ){
      return $objResponse->addAlert("No pude guardar la sección:". $DbConnection->getErrorsString());
    }
    
    Session::setMessage('Sección Guardada',SESSION_MESSAGE_SUCCESS);
    $objResponse->redirect(TO_WEB_ROOT ."/sections/admin/sections.php");
    return $objResponse;
  }
  $functions[] = 'saveSection';
  
    function saveContent($data) {
    $objResponse = new xajaxResponse();
    
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    
    $User = Session::getUser();
    if (!$User->hasPermission('/', all) ) {
      die();
    }
    
    if( ! XajaxHelper::saveRow($data,'content') ){
      return $objResponse->addAlert("No pude guardar el contenido:". $DbConnection->getErrorsString());
    }
    
    Session::setMessage('Contenido Guardado',SESSION_MESSAGE_SUCCESS);
    $objResponse->redirect(TO_WEB_ROOT ."/sections/admin/contents.php");
    return $objResponse;
  }
  $functions[] = 'saveContent';
  
  
  function deleteSale($data){
    
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    $DbConnection=DbConnection::getInstance();
    $User = Session::getUser();
    $objResponse = new xajaxResponse();
    
    if ( is_array($data) ) {
      $sale_id = $sale_id = (int)Utils::cleanToDb($data['sale_id']);
    } else {
      $sale_id = (int)Utils::cleanToDb($data);
    }
    
    if($sale_id) {
      die();
    }
    $Sale = new SaleModel($sale_id);
  	if ( !$Sale->delete() ){
	  return $objResponse->alert(t("Couldn't Delete %1%", $DbConnection->getLastError()));
	}
	Session::setMessage('Venta eliminada exitosamente', SESSION_MESSAGE_SUCCESS);
  	return $objResponse->redirect("index.php"); 
  }
  $functions[]='deleteSale';
  
 
  /**
   * Cancels a sale, restoring inventory.
   * @param array $data
   * @todo restore inventory
   */
  function cancelSale($sale_id){
    
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    $objResponse = new xajaxResponse();

    $User = Session::getUser();
    
    if(!$User->hasPermission('/', 'edit')) {
      die();
    }
    
    $sale_id = (int)Utils::cleanToDb($sale_id);
    
    if($sale_id==0) {
      die();
    }
    
    $Sale = new SaleModel($sale_id);
    
    if(!$Sale->load()) {
      return $objResponse->alert(t("That sale doesn't exist"));  
    }
    
    if ( !$Sale->cancel() ){
	  return $objResponse->alert(t("Couldn't Cancel %1%", $DbConnection->getLastError()));
	}
	
	Session::setMessage('Venta cancelada exitosamente', SESSION_MESSAGE_SUCCESS);
  	return $objResponse->redirect("view.php?sale_id=$sale_id"); 
  }
  $functions[]='cancelSale';
  
  
  
    /**
   * Cancels a sale, restoring inventory.
   * @param array $data
   * @todo restore inventory
   */
  function cancelSaleWithAuth($data){
    
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    $objResponse = new xajaxResponse();

    if( !$User = UserModel::getUserByName($data['user']) ) {
      return $objResponse->alert('Usuario o contraseña erronea.');
    }

    if( !$User->validatePassword($data['password']) ){
      return $objResponse->alert('Usuario o contraseña erronea.');
    }
    
    $sale_id = (int)Utils::cleanToDb($data['sale_id']);
    if($sale_id==0) {
      die();
    }
    
    $Sale = new SaleModel($sale_id);
    if(!$Sale->load()) {
      return $objResponse->alert(t("That sale doesn't exist"));  
    }
    
    if ( !$Sale->cancel() ){
	  return $objResponse->alert(t("Couldn't Cancel %1%", $DbConnection->getLastError()));
	}
	
	Session::setMessage('Venta cancelada exitosamente', SESSION_MESSAGE_SUCCESS);
  	return $objResponse->redirect("view.php?sale_id=$sale_id"); 
  }
  $functions[]='cancelSaleWithAuth';
  /**
   * Saves a Generic {@link Row} of Data, to be used at catalog
   *
   * @param Array $data
   * @return xajaxResponse
   *
   * @todo Custom error Message
   */
  function saveCatalogProduct($data)
  {    
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    
    $DbConnection = DbConnection::getInstance();
    $User = Session::getUser();
    
    $objResponse = new xajaxResponse();
    unset($data['table_name']);
    $product_id = $data['product_id'];
    
    if( !XajaxHelper::saveRow($data,'product') ){
      $objResponse->alert(   t("No se pudo guardar el producto:". $DbConnection->getErrorsString()));
      return $objResponse;
    }
    
    if($product_id==0) {
      $product_id = $DbConnection->getLastId();
      $objResponse->alert('El producto se ha guardado corrrectamente');
      $objResponse->redirect( TO_ROOT . "/catalog/edit_product.php?product_id=$product_id");
    } else {
      $objResponse->alert('El producto se ha actualizado corrrectamente');
    }
    return $objResponse;
  }
  $functions[] ='saveCatalogProduct';
  
  function searchClientSales($search) {
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    $objResponse = new xajaxResponse();
    
    $search = Utils::cleanToDb($search);
    $User = Session::getUser();
    
    $Table = new TablePattern();
    $Table->loadConfig('client_select',
      array(
      '_search' => $search
      )
    );
    $objResponse->assign('clients', 'innerHTML', $Table->getAsString());
    
    return $objResponse;
  }
  $functions[] = 'searchClientSales';
  
  function openSelectClientDialog($data) {
    $objResponse = new xajaxResponse();
    $User = Session::getUser();
    
    $Table = new TablePattern();
    $Table->loadConfig('client_select',
      array(
        '_search'=>''
      )
    );
    $table = $Table->getAsString();
    
    $string .='<h3>Seleccionar Cliente</h3>
    <form action=""><p>
    <label for="search">Buscar:</label>
    <input type="text" id="search" name="search" onkeyup="xajax_searchClientSales(this.value)"/></p>
    </form>';
    
    $string .= "<ul class=\"action\">\n";
    $string .= "<li>" . HelperPattern::createActionCall('javascript:closeOverlay();', 'Back', '','', 'images/toolbars/undo.png', false, true, true)."</li>\n";
    $string .= "<li>" . HelperPattern::createActionCall(TO_WEB_ROOT . 'sections/sales/edit_client.php?client_id=0', 'Añadir Cliente', '','', 'images/toolbars/add.png', false, true, true)."</li>";
    $string .= "</ul>\n";
    
    $string .= "<div id=\"clients\">" . $table . '</div>';
    
    $objResponse->assign('overlay', 'innerHTML', $string);
    $objResponse->script('showOverlay()');
    $objResponse->script("document.getElementById('search').focus();");
    
    return $objResponse;  
  }
  $functions[] = 'openSelectClientDialog';
  
  function selectClientSales($client_id){
    $objResponse = new xajaxResponse();
    $DbConnection = DbConnection::getInstance();
    $sql = "SELECT name FROM client WHERE client_id=$client_id";
    $client_name = $DbConnection->getOneValue($sql);
    
    $objResponse->assign('client_name', 'value', $client_name);
    $objResponse->assign('client_id', 'value', $client_id);
    $objResponse->script('closeOverlay();');
    return $objResponse;
  }
  $functions[]='selectClientSales';
  
  /**
   * Saves a Generic {@link Row} of Data, to be used at catalog
   *
   * @param Array $data
   * @return xajaxResponse
   *
   * @todo Custom error Message
   */
  function saveSale($data)
  {    
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    
    $DbConnection = DbConnection::getInstance();
    $User = Session::getUser();
    
    $objResponse = new xajaxResponse();
    unset($data['client_name']);
    $sale_id = $data['sale_id'];
    
    if ( empty($data['client_id']) ) {
      return $objResponse->alert("Debes seleccionar un cliente primero");
    }
    if ( empty($data['dealer_id']) ) {
      return $objResponse->alert("Debes seleccionar un vendedor primero");
    }
    
    if( !XajaxHelper::saveRow($data,'sale') ){
      return $objResponse->alert("No se pudo guardar la venta:".$DbConnection->getLastError());
    }
    
    if($sale_id==0) {
      $sale_id = $DbConnection->getLastId();
      $objResponse->redirect("edit.php?sale_id=$sale_id");
    } else {
      $objResponse->alert('La venta se ha actualizado corrrectamente');
    }
    return $objResponse;
  }
  $functions[] ='saveSale';
  
  function searchSale($search) {
    if ( !Session::assertLoggedIn() ) {
      die();
    }
    $objResponse = new xajaxResponse();
    
    $search = Utils::cleanToDb($search);
    $User = Session::getUser();
    
    $Table = new TablePattern();
    $Table->loadConfig('search_sale', array('search' => $search));
    $objResponse->assign('sales', 'innerHTML', $Table->getAsString());
    return $objResponse;
  }
  $functions[] = 'searchSale';
  
  function openAskCancelSaleDialog($sale_id) {
    $objResponse = new xajaxResponse();
    
    if ( !Session::assertLoggedIn() ) {
      die();
    }

    $string .='<h3>Autorización</h3>
    <form action="" id="auth" name="auth">
    <p>
      <label for="user">Usuario:</label>
      <input type="text" id="user" name="user"/><br/>
      <label for="password">Contraseña:</label>
      <input type="password" id="password" name="password"/>
      <input type="hidden" id="sale_id" name="sale_id" value="'.$sale_id.'"/>
    </p>
    </form>';
    
    $string .= "<ul class=\"action\">\n";
    $string .= "<li>" . HelperPattern::createActionCall('javascript:closeOverlay();', 'Back', '','', 'images/toolbars/undo.png', false, true, true)."</li>\n";
    $string .= "<li>" . HelperPattern::createActionCall('javascript:void(xajax_cancelSaleWithAuth(xajax.getFormValues(\'auth\')));', 'Cancelar Venta', '','', 'images/toolbars/cancel.png', false, true, true)."</li>";
    $string .= "</ul>\n";
    $objResponse->assign('overlay', 'innerHTML', $string );
    $objResponse->script('showOverlay();');
    
    return $objResponse;
  }
  $functions[] ='openAskCancelSaleDialog';
  
  foreach($functions AS $function){
    $xajax->registerFunction($function);
  }
  $xajax->processRequests();