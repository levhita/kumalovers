<?php
function assertLoggedIn() {
  if ( !CrowdterSession::assertLoggedIn() ) {
    PagePattern::goToPage(TO_ROOT . '/account/login_form.php', t('User not logged in'), GOTO_MESSAGE_WARNING);
  }
}