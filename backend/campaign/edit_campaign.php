<?php
define('TO_ROOT', '..');
require TO_ROOT . "/includes/main.inc.php";

assertLoggedIn();
$Customer = CrowdterSession::getUser();

$campaign_id = GetRequest::zeroParameter('campaign_id', 'int');
if ( !$Campaign = $Customer->getCampaign($campaign_id) ) {
  $Campaign = new CampaignModel(0);
  $Campaign->data['customer_id'] = $Customer->getId();
} 

$CampaignForm = new FormPattern();
$CampaignForm->setRow($Campaign);
//$CampaignForm->loadConfig('default');

$Page = new PagePattern('Campaign');

$Page->assign('Campaign', $Campaign);
$Page->assign('campaign_form', $CampaignForm->getAsString());
$Page->display(); 