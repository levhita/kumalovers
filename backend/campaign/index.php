<?php
define('TO_ROOT', '..');
require TO_ROOT . "/includes/main.inc.php";

assertLoggedIn();

$Customer = CrowdterSession::getUser();

$campaigns = $Customer->getAllCampaigns();

$Page = new PagePattern('Campaigns');

$Page->assign('campaigns', $campaigns);
$Page->display(); 