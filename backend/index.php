<?php
define('TO_ROOT', '.');
include TO_ROOT . "/includes/main.inc.php";

$Page = new PagePattern();
$Page->setPageName('Home');
if( !Session::assertLoggedIn() ) {
  $Page->goToPage('sections/users/login.php');
}

$Page->display();