<?php
define('TO_ROOT', '../..');
include TO_ROOT . "/includes/main.inc.php";
assertLoggedIn();
$Page = new PagePattern('Sections');

$Table = new TablePattern();
$Table->loadConfig('section');
$Page->assign('sections', $Table->getAsString());

$Page->display();