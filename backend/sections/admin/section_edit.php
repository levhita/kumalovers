<?php
define('TO_ROOT', '../..');
include TO_ROOT . "/includes/main.inc.php";
include TO_ROOT . "/includes/ajax_server.php";

assertLoggedIn();
$Page = new PagePattern('Secciones');

$section_id = GetRequest::zeroParameter('section_id', 'int'); 
$Section = new RowModel('section', $section_id);
if($section_id != 0) {
  $Section ->load();
}

$Form = new FormPattern();
$Form->setRow($Section);

$Form->loadConfig('section', false);

$Page->assign('section', $Form->getAsString());

$Page->display();