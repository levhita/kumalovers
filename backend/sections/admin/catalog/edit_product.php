<?php
define('TO_ROOT', '../../..');
include TO_ROOT . "/includes/main.inc.php";
include TO_ROOT . "/includes/ajax_server.php";

$Page = new PagePattern('Editar Producto');
$Request = GetRequest::getInstance();

$product_id = $Request->zeroParameter('product_id');

if($product_id!=0){
  $Product = new ProductModel((int)$product_id);
  $Product->load();
} else {
  $Product = new ProductModel(0);
}

$config = ConfigParser::parsea_mesta(TO_ROOT . '/configs/models/product_catalog_many.yaml');

$Form = new FormPattern();
$Form->setRow($Product);
$Form->loadConfig($config['_form_config'], false);
$aux = array(
	'label' => t('Table name'),	
	'type' => 'hidden',	
	'value' => 'product'
);
$Form->insertField('table_name', $aux , "product_id", 'before');
$Form->hideField("product_id");

$Table= new TablePattern();
$Table->loadConfig($config['_list_config'], array('product_id'=>$product_id));

$Page->assign('Form', $Form);
if($product_id!=0) {
  $Page->assign('table', $Table->getAsString());
}

$Page->display();