<?php
  define('TO_ROOT', '../../..');
  include TO_ROOT . "/includes/main.inc.php";

  assertLoggedIn();
  $User = Session::getUser();
  if (!$User->hasPermission('/', all) ) {
    die();
  }

  $filename = (isset($_FILES['logo']['name']))?basename($_FILES['logo']['name']):'';
  if ( !empty($filename) ) {
    $match=FALSE;
    $allowed_extensions = array('png', 'jpg', 'gif', 'jpeg');
    foreach($allowed_extensions AS $extension){
      if ( preg_match("/\.$extension$/i", $filename) > 0) {
        $match=TRUE;
      }
    }
    if(!$match){
      PagePattern::goToPage('list_table.php?table_name=campaign', t('Invalid image'), GOTO_MESSAGE_ERROR);
    }
  }
  
  $Request = PostRequest::getInstance();

  $campaign_id = 0;
  if(isset($Request->campaign_id) && 0!=(int)$Request->campaign_id) {
    $campaign_id=(int)$Request->campaign_id;
  }
  $Campaign = new CampaignModel($campaign_id);
  
  if ($campaign_id != 0) {
    $Campaign->load();
  }

  $Campaign->data = $Request->getParams();  
  unset($Campaign->data['table_name']);
  
  if ( !$Campaign->save() ) {
    PagePattern::goToPage('edit.php?table_name=campaign&campaign_id='. $campaign_id, t("Couldn't save the Campaign"), GOTO_MESSAGE_ERROR);
  }
	$Campaign->load();
	
	if ( !empty($filename) ) {
	  $Config = Config::getInstance();
	  $temp_file = $_FILES['logo']['tmp_name'];
	  $upload_dir = TO_FRONTEND . $Config->image_folder;
	  $final_name = $Campaign->getId() . ".jpg";
	  $upload_file = $upload_dir . $final_name;
	  $command = TO_ROOT . "/includes/resize.sh $Config->image_logo_width $temp_file $upload_file";
	  exec($command);
	  
	  $Campaign->data['logo'] = $final_name;
	  $Campaign->save();
	}
	PagePattern::goToPage('list_table.php?table_name=campaign', t('Campaign Saved'), GOTO_MESSAGE_SUCCESS);