<?php
define('TO_ROOT', '../../..');
include TO_ROOT . "/includes/main.inc.php";
include TO_ROOT . "/includes/ajax_server.php";

$DbConnection = DbConnection::getInstance();
$Page = new PagePattern('Edit');

$permission_id = (empty($_GET["permission_id"])) ? 0 : Utils::cleanToDb($_GET["permission_id"]);

$general_config = ConfigParser::parsea_mesta(TO_ROOT."/configs/models/catalog.yaml");
$config = $general_config['permission'];

$Page->setPageName($config['_singular']);

$Permission = new PermissionModel((int)$permission_id);
if ($permission_id != 0) {
  $Permission->load();
}

$Form=new FormPattern();
$Form->setRow($Permission);
$Form->loadConfig("permission_catalog", false);

$aux = array(
	'label' => t('Table name'),	
	'type' => 'hidden',	
	'value' => 'permission'
);
$Form->insertField('table_name', $aux , "permission_id", 'before');

if ($permission_id!=0) {
  $permission_type_id = $Permission->getPermissionTypeId();
  $Form->setFieldProperty('permission_type_id', 'value', $permission_type_id);
}

$Page->assign('form', $Form->getAsString());

$Page->display();