<?php
define('TO_ROOT', '../../..');
include TO_ROOT . "/includes/main.inc.php";

$Page = new PagePattern();

if( !empty($_GET['table_name']) ) {
 $table_name = $_GET['table_name'];
} else {
 $Page->goToPage('index.php',t("That table doesn't exists"));
}

$general_config = ConfigParser::parsea_mesta(TO_ROOT."/configs/models/catalog.yaml");
$config = $general_config[$table_name];

$Page->setPageName("{$config['_plural']}");

$DbConnection = DbConnection::getInstance();
$list_config = "{$table_name}_catalog";
if(isset($config['_list_config'])) {
  $list_config = $config['_list_config'];
}
$Table = new TablePattern();
$Table->loadConfig($list_config);

$Page->assign('table', $Table->getAsString());
$Page->display();