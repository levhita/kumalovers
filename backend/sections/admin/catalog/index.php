<?php
define('TO_ROOT', '../../..');
include TO_ROOT . "/includes/main.inc.php";

$Page = new ListingPattern('Catalog');
$DbConnection = DbConnection::getInstance();

$config = ConfigParser::parsea_mesta(TO_ROOT."/configs/models/catalog.yaml");

//Logger::log('config',print_r($config,1),LOGGER_NOTICE);

$rows_number = count($config);

$rows = array();
foreach($config AS $key=>$value) {
  if(substr($key, 0, 1) != '_'){
    $rows[] = array (
        'table_name'=>$value['_table'],
        'plural'=> $value['_plural'],
        'singular' => $value['_singular'],
        'description'=>$value['_description'],
        'action'=>$value['_action'],
        );
  }
}

$Page->setRows($rows);

$Page->setName('plural','Tabla');
$Page->hideField('table_name');
$Page->hideField('singular');
$Page->hideField('action');
$Page->addAction('table_name','list_table.php','Ver Registros', 'images/toolbars/list.png');
$Page->display();