
<?php
define('TO_ROOT', '../../..');
include TO_ROOT . "/includes/main.inc.php";
include TO_ROOT . "/includes/ajax_server.php";

$Page = new PagePattern('Edit');

$table_name = $_GET['table_name'];
$id   = (empty($_GET["{$table_name}_id"])) ? 0 : Utils::cleanToDb($_GET["{$table_name}_id"]);

$general_config = ConfigParser::parsea_mesta(TO_ROOT."/configs/models/catalog.yaml");
$config = $general_config[$table_name];

$Page->setPageName($config['_singular']);
$User = Session::getUser();
$User->hasPermission('/catalog/materiales', 'read');

$form_config = "{$table_name}_catalog";
if(isset($config['_form_config'])) {
  $form_config = $config['_form_config'];
}

$Row = new RowModel($table_name, (int)$id, $DbConnection);
$Row->load();
$Form=new FormPattern();
$Form->setRow($Row);
$Form->loadConfig($form_config, false);
$Page->addJavascript($Form->createDependentJavascript());

$aux = array(
	'label' => t('Table name'),	
	'type' => 'hidden',	
	'value' => $table_name
);

$Form->insertField('table_name', $aux , "{$table_name}_id", 'before');
$Form->hideField("{$table_name}_id");

$Page->assign('form', $Form->getAsString());

$Page->display();