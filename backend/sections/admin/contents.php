<?php
define('TO_ROOT', '../..');
include TO_ROOT . "/includes/main.inc.php";
assertLoggedIn();
$Page = new PagePattern('contents');

$Table = new TablePattern();
$Table->loadConfig('content');
$Page->assign('contents', $Table->getAsString());

$Page->display();