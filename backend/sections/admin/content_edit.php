<?php
define('TO_ROOT', '../..');
include TO_ROOT . "/includes/main.inc.php";
include TO_ROOT . "/includes/ajax_server.php";

assertLoggedIn();
$Page = new PagePattern('Contenido');

$content_id = GetRequest::zeroParameter('content_id', 'int'); 
$Content = new RowModel('content', $content_id);
if($content_id != 0) {
  $Content->load();
}

$Form = new FormPattern();
$Form->setRow($Content);

$Form->loadConfig('content', false);

$Page->assign('content', $Form->getAsString());

$Page->display();