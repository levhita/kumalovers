<?php
define('TO_ROOT', '../..');
include TO_ROOT . "/includes/main.inc.php";

$Page = new PagePattern();
$Page->setPageName('Update Triggers');
if( !Session::assertLoggedIn() ) {
  $Page->goToPage('sections/users/login.php');
}

$User = Session::getUser();
if( !$User->hasPermission('/', 'all') ) {
  die();
}
$Request = GetRequest::getInstance();

$template_name = "trigger_template";
if(isset($Request->drop) && $Request->drop=='1') {
  $template_name = "drop_trigger_template";
}

$trigger_template = file_get_contents(TO_ROOT . "/../documentation/$template_name.sql");
$DbConnection = DbConnection::getInstance();
$tables = $DbConnection->getOneColumn("SHOW TABLES");
$results=array();
$end_sql ="";
foreach($tables AS $table) {
  
  $main_sql = str_replace('{table}', $table, $trigger_template);
  $end_sql .= "$main_sql\n";
  
  $results[$table] = array('main_sql' =>  $main_sql);
  $single_sqls = explode(";\n", $main_sql);
  array_pop($single_sqls);
  foreach ($single_sqls AS $sql) {
    $result = array( 'sql' => $sql);
    if( !$DbConnection->executeQuery($sql) ) {
      $result['results'] = $DbConnection->getLastError();
    } else {
      $result['results'] = 'Done';
    }
    $results[$table][]=$result;
  }
}
$Page->assign('results', $results);
$Page->assign('end_sql', $end_sql);
$Page->display();