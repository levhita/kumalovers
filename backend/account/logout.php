<?php
define('TO_ROOT', '..');
require TO_ROOT . "/includes/main.inc.php";

session_destroy();
PagePattern::goToPage('/account/login_form.php', t('You had been sucessfully logged out'), GOTO_MESSAGE_SUCCESS);