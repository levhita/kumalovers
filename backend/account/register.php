<?php
define('TO_ROOT', '.');
include TO_ROOT . "/includes/main.inc.php";

if (!CrowdterSession::assertLoggedIn()) {
  PagePattern::goToPage('/campaigns/');
}

$Page = new PagePattern();
$Page->setPageName('Register');


$Page->display();