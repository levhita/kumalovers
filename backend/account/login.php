<?php
define('TO_ROOT', '..');
require TO_ROOT . "/includes/main.inc.php";

if (CrowdterSession::assertLoggedIn()) {
  PagePattern::goToPage("../campaign/");
}

require THAFRAME .'/vendors/tmhOAuth/tmhOAuth.php';
require THAFRAME .'/vendors/tmhOAuth/tmhUtilities.php';

$Config = Config::getInstance();

$tmhOAuth = new tmhOAuth(array(
  'consumer_key'    => $Config->crowdter_consumer_key,
  'consumer_secret' => $Config->crowdter_consumer_secret,
));

$here = tmhUtilities::php_self();

function outputError($tmhOAuth) {
  echo 'Error: ' . $tmhOAuth->response['response'] . PHP_EOL;
  tmhUtilities::pr($tmhOAuth);
}

if (isset($_SESSION['access_token']['oauth_token'])) {
  $tmhOAuth->config['user_token']  = $_SESSION['access_token']['oauth_token'];
  $tmhOAuth->config['user_secret'] = $_SESSION['access_token']['oauth_token_secret'];

  $code = $tmhOAuth->request('GET', $tmhOAuth->url('1/account/verify_credentials'));
 
  if ($code == 200) {
    $profile_data = json_decode($tmhOAuth->response['response']);
    $Customer = CustomerModel::CreateCustomer($profile_data,  $tmhOAuth->config['user_token'], $tmhOAuth->config['user_secret']);
    CrowdterSession::setAsLoggedIn($Customer->getId());
    PagePattern::goToPage('../campaign/');
  } else {
    outputError($tmhOAuth);
  }
} elseif (isset($_REQUEST['oauth_verifier'])) {
  // we're being called back by Twitter  
  $tmhOAuth->config['user_token']  = $_SESSION['oauth']['oauth_token'];
  $tmhOAuth->config['user_secret'] = $_SESSION['oauth']['oauth_token_secret'];

  $code = $tmhOAuth->request('POST', $tmhOAuth->url('oauth/access_token', ''), array(
    'oauth_verifier' => $_REQUEST['oauth_verifier']
  ));

  if ($code == 200) {
    $_SESSION['access_token'] = $tmhOAuth->extract_params($tmhOAuth->response['response']);
    unset($_SESSION['oauth']);
    header("Location: {$here}");
  } else {
    outputError($tmhOAuth);
  }

} else {
  //start the OAuth dance
  $params = array('oauth_callback' => $here);
  $code = $tmhOAuth->request('POST', $tmhOAuth->url('oauth/request_token', ''), $params);
  if ($code == 200) {
    $_SESSION['oauth'] = $tmhOAuth->extract_params($tmhOAuth->response['response']);
    $authurl = $tmhOAuth->url("oauth/authenticate", '') .  "?oauth_token={$_SESSION['oauth']['oauth_token']}";
    header("Location: {$authurl}");
  } else {
    outputError($tmhOAuth);
  }
}