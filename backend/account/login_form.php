<?php
define('TO_ROOT', '..');
require TO_ROOT . "/includes/main.inc.php";

if (CrowdterSession::assertLoggedIn()) {
  PagePattern::goToPage("../campaign/");
}

$Page = new PagePattern('Login');
$Page->display();